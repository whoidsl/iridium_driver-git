"""
Polls the sentry.sat.coms gmail account for new messages every 30 seconds.
"""

import sys

if sys.version_info < (3, 4):
    sys.exit("Requires python 3.4 or newer")

import asyncio
import logging
import functools
from contextlib import suppress
import email

# Hacky import to get around trying the example without installing the package first
try:
    from iridium.transports import (XeosEmailTransport, create_xeos_email_endpoint)
    from iridium.protocols import XeosProtocol
    from iridium.parsing import parse_xeos_email
except ImportError:
    import os

    sys.path.insert(0, os.path.realpath(os.path.join(os.path.dirname(__file__), "..")))
    from iridium.transports import (XeosEmailTransport, create_xeos_email_endpoint)
    from iridium.protocols import XeosProtocol
    from iridium.parsing import parse_xeos_email


class EmailPrinter(object):
    """
    Simple "handler" that just prints xeos emails as parsed dictionaries to stdout

    """

    def handle_iridium_email_payload(self, msg: email.message.EmailMessage):
        r"""
        @param msg:  Email message
        @return: None
        """

        # Try parsing the message
        parsed_msg = parse_xeos_email(msg)
        if parsed_msg is None:
            return

        print(parsed_msg)


def main():
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("password", type=str, help="password for sentry.sat.coms@gmail.com")
    parser.add_argument("-i", "--interval", type=int, metavar="SECONDS", help="Polling interval for new messages",
                        default=30)

    args = parser.parse_args()

    # Print log messages too.
    logging.basicConfig(level=logging.DEBUG)

    # Get our event loop.
    loop = asyncio.get_event_loop()

    (transport, protocol) = loop.run_until_complete(
        create_xeos_email_endpoint(
            lambda: XeosProtocol(),
            imap_addr=("imap.gmail.com", 993),
            imap_login=('sentry.sat.coms@gmail.com', args.password),
            smtp_addr=('smtp.gmail.com', 587),
            smtp_login=('sentry.sat.coms@gmail.com', args.password),
            recipient='bademail@nowehere.com'))

    # Create and add our handler
    service = EmailPrinter()

    # The handler needs to be a callable with a single argument (the message),
    # functools.partial allows us to add additional arguments... but we don't have any in this case.
    # In general you'll use this when your handler is part of a larger class
    protocol.attach_handler(functools.partial(service.handle_iridium_email_payload))

    # Start polling the email transport for new messages
    transport.start_polling(30)

    # Start the event loop and block until keyboard interrupt is fired, then clean up.
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        print("\nCaught Ctrl-C")
    finally:
        # Stop polling
        transport.stop_polling()
        # Cancel all remaining pending tasks.
        pending = asyncio.Task.all_tasks()
        for task in pending:
            task.cancel()
            # Now we should await task to execute it's cancellation.
            # Cancelled task raises asyncio.CancelledError that we can suppress:
            with suppress(asyncio.CancelledError):
                loop.run_until_complete(task)
        # Stop the event loop.
        loop.stop()

    print("Done!")


if __name__ == '__main__':
    main()
