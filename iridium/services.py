import asyncio
import logging
import functools
import codecs

from iridium.protocols import XeosProtocol, CommandProtocol
from iridium.transports import XeosBaseTransport
from iridium.parsing import (xeos_fix_dict_to_gga, parse_xeos_email, parse_xeos_fix_binary)

import email
import re
from datetime import datetime
from datetime import timedelta

XEOS_BEACON_STATUS = ['NORMAL','STARTUP','SURFACED','SUBMERGED','LOW_BATT']

class IridiumService(object):
    r"""
    Service for interacting with a remote iridium beacon through email.
    """

    def __init__(
            self, xeos_protocol: XeosProtocol, xeos_transport: XeosBaseTransport,
            udp_protocol: CommandProtocol, udp_transport: asyncio.DatagramTransport,
            forwarding_addr: (str, int), beacon, unlock, discard_old: bool, discard_gps: bool, 
            prepend_timestamp: bool, discard_age: int, strip_timestamp: bool, loopback: bool):
        r"""

        @param protocol:
        @param transport:
        """

        self.xeos_transport = xeos_transport
        xeos_protocol.attach_handler(functools.partial(self.handle_xeos_message))

        self.udp_transport = udp_transport
        udp_protocol.attach_handler(functools.partial(self.handle_udp_message))

        self.forwarding_addr = forwarding_addr

        self.beacon = beacon
        self.unlock = unlock
        self.discard_old = discard_old
        self.discard_gps = discard_gps
        self.prepend_timestamp = prepend_timestamp
        self.discard_age = timedelta(seconds=discard_age)
        self.strip_timestamp = strip_timestamp
        self.loopback = loopback

        self.beacon_status = None # unknown until response to $status is parsed.

        self._log = logging.getLogger(__name__)
        self._log.info("Local command address: %s", udp_transport.get_extra_info('socket').getsockname())
        

    def handle_xeos_message(self, msg):
        r"""

        @param msg:
        @return:
        """

        if isinstance(msg, email.message.Message):
            self.handle_iridium_email_payload(msg)
            return

        # Otherwise it's a string or bytes -- it's data from a local iridium.
        if isinstance(msg, bytes):
            try:
                msg = msg.decode()
            except UnicodeDecodeError:
                self._log.error("Unable to decode some bytes from the serial port.  Bad connection?")
                return

        self.handle_iridium_local_payload(msg)

    def handle_iridium_email_payload(self, msg):
        r"""

        @param msg:
        @return:
        """

        # Try parsing the message
        parsed_msg = parse_xeos_email(msg)
        if parsed_msg is None:
            self._log.warning("Invalid Xeos iridium email: %r", str(msg))
            return

        # Ignore data from other beacons
        if parsed_msg['beacon'] != self.beacon:
            self._log.info("Email from some other beacon (%r) ignored.", str(parsed_msg['beacon']))
            return

        msg_type = parsed_msg.get('type', None)

        if msg_type == 'fix':
            self.new_xeos_gps_fix(parsed_msg)
        elif msg_type == 'powerup':
            self.new_xeos_powerup(parsed_msg)
        elif msg_type == 'devdata':
            self.new_xeos_devdata(parsed_msg)

    def handle_iridium_local_payload(self, msg):
        r"""

        @return:
        """
        
        # Downlinked (MT) data from email looks like:
        # $unlock 1234
        # $outport A
        # unlocked
        # payload line 1
        # payload line 2 
        # ...
        #
        # MT data from another beacon (MO from the perspective of that beacon) looks like:
        # devData X/S/X/Y\r payload
        # Where X, S, X, Y are defined in the manual and unused here.
        #
        # Only forward message payloads that conform to the above.
        # Supports only single-line messages for forwarding.
        # Diagnostic messages terminate on the console.  
        fwd = False
        last = False
        for line in [x for x in msg.splitlines(keepends=True) if len(x.strip()) > 0]:
            
            self._log.info(line.rstrip())

            # Only forward the last message from the Iridium queue.
            # iRx:+SBDIX: <MO status>, <MOMSN>, <MT status>, <MTMSN>, <MT length>, <MT queued>
            if "+SBDIX" in line:
                try:
                    args = line.split(':',2)[2].split(',')
                    if int(args[0]) in range(0,4) and int(args[2])==1 and int(args[5])==0:
                        last = True
                        continue
                except:
                    continue

            if 'devData' in line or 'unlocked' in line:
                fwd = True
                continue

            if fwd and (last or not self.discard_old):
                msg_dict = dict(timestamp=None, type='devdata', port=None, seq=None, part=None, total=None,
                                payload=line.splitlines())
                self.new_xeos_devdata(msg_dict)

                # All subsequent lines regarded as diagnostic info and ignored.
                return

            # Forward all or discard all GPS fixes.  These have an internal timestamp so latency 
            # can be dealt with by downstream applications.
            # Binary GPS fixes sent from other beacons appear only in diagnostic output. 
            # Lines containing GPS appear like <timestamp> Ird Rxd(24 bytes) FE ...
            if not self.discard_gps:
                m = re.search('Ird Rxd\([0-9]* bytes\) FE',line)
                if m:
                    self._log.info("Raw payload from another beacon: %s",line.rstrip())
                    parsed_msg = parse_xeos_fix_binary(line[m.end()-2:])
                    self._log.info("Parsed payload: %s", parsed_msg)
                    self.new_xeos_gps_fix(parsed_msg)
                    return
                
            # Look for response to $status.  This indicates whether the beacon thinks it is
            # submerged or on the surface.  e.g.
            # Status:Up=3590 EvtCfg=1 TMde=1 RMde=1 vb=23.906/23.070 Tlt=N;
            m = re.search('^Status:.*EvtCfg=',line)
            if m:
                try:
                    self.beacon_status = XEOS_BEACON_STATUS[int(line[m.end():m.end()+1])]
                    self._log.info("Beacon status interpreted as: %s",self.beacon_status)
                    self.xeos_transport.set_beacon_status(self.beacon_status)
                except:
                    self._log.info("Unknown beacon status: %s",line.rstrip())

    def new_xeos_gps_fix(self, msg_dict):
        """
        Called when we receive a new gps fix from the iridium.

        @param msg_dict:  GPS fix data
        @return:

        `msg_dict` will have the following:

            Key         Type                       Desc
            type        str                        Set to 'fix'
            beacon_id:  str                        ID of Xeos beacon
            timestamp   class:`datetime.datetime`  Timestamp of fix
            battery     float                      Battery level of beacon
            lat         float                      Fix latitude (+N)
            lon         float                      Fix longitude (+E)

        """

        spoofed_gga = xeos_fix_dict_to_gga(msg_dict)
        self._log.info("Sending generated GPGGA from xeos fix: %r", spoofed_gga)
        self.udp_transport.sendto(spoofed_gga.encode(), self.forwarding_addr)

    def new_xeos_powerup(self, msg_dict):
        r"""
        Called when we receive a new power-up message

        @param msg_dict:
        @return:
        """

        self._log.info("Recieved Xeos powerup message: %s", msg_dict)

    def new_xeos_devdata(self, msg_dict):
        r"""

        @param msg_dict:
        @return:

        `msg_dict` will have the following:

            Key         Type                       Desc
            type        str                        Set to 'fix'
            timestamp   class:`datetime.datetime`  Timestamp of fix
            port        str                        Iridium port that received message
            seq         int                        Message sequence number
            part        int                        This message part
            total       int                        Total parts in this message
            payload     [str]                      Data sent from remote iridium
        """

        self._log.info("New data from remote iridium: %r", msg_dict)

        # Supports multi-line messages but each line is forwarded separately
        for line in [x for x in msg_dict['payload'] if len(x.strip()) > 0]:
        
            # Examine timestamps prepended by an originating driver instance and discard those
            # older than a threshold.  Strip the timestamp (so that communication appears transparent)
            # Messages not bearing a timestamp are unaffected.

            try:
                ts = datetime.strptime(line[:23],"ITS %Y-%m-%d %H:%M:%S")
            except ValueError:
                if self.discard_age:
                    self._log.warning("Configured to discard by age but incoming message has no timestamp.")
                if self.strip_timestamp:
                    self._log.warning("Configured to strip prepended timestamp but incoming message has no timestamp.")
            else:
                dt = datetime.utcnow() - ts
                if self.discard_age:
                    if dt >= self.discard_age:
                        self._log.info("Message discarded as too old.  Age: %d s" % dt.total_seconds())
                        continue
                if self.strip_timestamp:
                    line = line[24:] # strip prepended timestamp

            # Loopback if configured to do so.  Use UDP handler to add
            # any further options.
            if self.loopback:
                self._log.info("Looping back message received over Iridium: %r", line)
                self.handle_udp_message('ILB ' + line)

            # Send out payload strings as is onto the network
            unescaped_line = line.encode('utf-8').decode('unicode_escape')
            self.udp_transport.sendto(unescaped_line.encode(), self.forwarding_addr)

    def handle_udp_message(self, payload: str):
        r"""

        @param payload:
        @return:
        """

        escaped_payload = payload.encode('unicode_escape').decode('utf-8')

        # prepend timestamp if configured to do so.  Does not check for 270 byte ISU Rx maximum.
        # Tx max is 340 but this can only be Rx'd over email.
        if self.prepend_timestamp:
            ts = datetime.strftime(datetime.utcnow(),"%Y-%m-%d %H:%M:%S")
            escaped_payload = 'ITS ' + ts + ' ' + escaped_payload

        # Send to beacon for uplink.
        self.xeos_transport.sendto_beacon(beacon=self.beacon, unlock=self.unlock, payload=escaped_payload)



        
