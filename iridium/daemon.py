r"""
Sentry Xeos Iridium driver.


"""
import logging
import asyncio
from contextlib import suppress
import configparser
import os
from datetime import datetime
import time

import iridium.protocols
import iridium.transports
import iridium.services

import socket

from iridium.dsl_log_handler import DslLogFileHandler

DESCRIPTION = r"""
Sentry Iridium Driver
"""

# Default values for
DEFAULT_VALUES = dict(
    imap="imap.gmail.com:993",
    smtp="smtp.gmail.com:587",
    recipient="data@sbd.iridium.com",
    interval=180,
    baud=57600,
    listen="0.0.0.0:53341",
    remote="127.0.0.1:53340",
    mailbox_check=False,
    discard_old=False,
    discard_gps=False,
    prepend_timestamp=False,
    discard_age=0,
    strip_timestamp=False,
    loopback=False,
    logdir=None,
    xeos_cfg="TOPSIDE"
)


XEOS_CFG = dict(
    TOPSIDE = [
        '$timer GPS 0 1d\r',  # effectively neuter GPS.
        '$timer SBD 0 1d\r',
        '$timer GPS 1 1d\r',
        '$timer SBD 1 1d\r',
        '$timer GPS 2 1d\r',
        '$timer SBD 2 1d\r',
        '$timer GPS 3 1h\r',
        '$timer SBD 3 1h\r',
        '$timer GPS 4 1h\r',
        '$timer SBD 4 1h\r',
        '$msfMM 1\r',
        '$iSbdMaxQty 15\r'],
    SUBSEA = [
        '$timer GPS 0 5m\r', # fastest internal rate possible (5 min)
        '$timer SBD 0 5m\r',
        '$timer GPS 1 5m\r',
        '$timer SBD 1 5m\r',
        '$timer GPS 2 5m\r',
        '$timer SBD 2 5m\r',
        '$timer GPS 3 1h\r', # slow down attempts subsea.
        '$timer SBD 3 1h\r',
        '$timer GPS 4 1h\r',
        '$timer SBD 4 1h\r',
        '$msfMM 1\r',
        '$iSbdMaxQty 15\r']
)
     
def read_config_file(file):
    r"""
    Parse an iridium driver config.

    @param file:  Config file to parse

    @return: dict

    The config file will have two sections.

    [iridium] section:

    - General options:
        - beacon:  Beacon IMEI ID string
        - unlock:  Unique unlock code
        - interval: Time between synchronous actions including scanning for new
                    messages (email) and Iridium MT mailbox check (serial)
        - logdir: Where to log console output.  Comment for no logging.

    - Xeos Forward related options:
        - email: Email address to use for Xeos Forward email link
        - password:  Password for the email account.
        - imap:  IMAP (incoming) server address:port string.
        - smtp:  SMTP (outgoing) server address:port string
        - recipient: Email address to send iridium data to.

    - Serial port related options:
        - port: Serial port of iridium beacon
        - baud: Baudrate of serial comms
    
    - Serially connected beacons options
        - discard_old: Discard or forward messages on the gateway when additional messages are queued
        - discard_gps: Discard binary GPS messages from another beacon
        - prepend_timestamp: Prepend a timestamp to uplinked messages 
        - discard_age: Discard messages by age.  Only works if the transmitting end is configured to 
                       prepend a timestamp.  
        - strip_timestamp: Strip prepended timestamp.
        - loopback: Loopback a message received over Iridium

    NOTE:  Only 'email' OR 'port' may be specified in this section to indicate whether
    the beacon is local (connected through a serial port) or remote (using the Xeos Forward
    email service).  Specifiying both will raise an error.

    NOTE: 'interval' specifies the interval between synchronous actions in either mode but the actions differ.

    [network] section:

        - listen: UDP address:port string to listen for network traffic
        - remote: UDP address:port string to forward data received from the iridium on.

    Example:

    >>> config_file = "\n".join([
    ... "[iridium]",
    ... "email=sentry.sat.coms@gmail.com",
    ... "imap=imap.gmail.com:993",
    ... "smtp=smtp.gmail.com:587",
    ... "password=email_password",
    ... "interval=30",
    ... "beacon=342108215324",
    ... "unlock=32145243",
    ... "",
    ... "[network]",
    ... "listen=0.0.0.0:53341",
    ... "remote=127.0.0.1:53340"])

    >>> config = read_config_file(config_file)
    >>> assert(config['email'] == 'sentry.sat.coms@gmail.com')
    >>> assert(config['imap'] == 'imap.gmail.com:993')
    >>> assert(config['smtp'] == 'smtp.gmail.com:587')
    >>> assert(config['password'] == 'email_password')
    >>> assert(config['interval'] == 30)
    >>> assert(config['beacon'] == '342108215324')
    >>> assert(config['unlock'] == '32145243')
    >>> assert(config['listen'] == '0.0.0.0:53341')
    >>> assert(config['remote'] == '127.0.0.1:53340')
    """

    parser = configparser.ConfigParser()

    if os.path.exists(file):
        if file not in parser.read(file):
            raise ValueError("Could not read config file: %s" % (file,))
    else:
        parser.read_string(file)

    # Section matches are case sensitive so we'll do a little snooping to figure out
    # what the actual section names in the file are.
    iridium_section = None
    for section in parser.sections():
        if section.strip().lower() == 'iridium':
            iridium_section = section
            break
    if iridium_section is None:
        raise ValueError("Config file has no 'iridium' section")

    # Now do the same for the 'network' section
    network_section = None
    for section in parser.sections():
        if section.strip().lower() == 'network':
            network_section = section
            break
    if network_section is None:
        raise ValueError("Config file has no 'network' section")

    # Start grabbing config data
    config = {}

    # Get the actual config entry from the parser
    network_section = parser[network_section]
    config['remote'] = network_section.get('remote', DEFAULT_VALUES['remote'])
    config['listen'] = network_section.get('listen', DEFAULT_VALUES['listen'])

    iridium_section = parser[iridium_section]

    if ('email' in iridium_section) and ('port' in iridium_section):
        raise ValueError("Cannot specifiy both 'email' and 'port' iridium configuration keys. Choose one.")

    if 'email' in iridium_section:
        config['email'] = iridium_section.get('email')
        config['imap'] = iridium_section.get('imap', DEFAULT_VALUES['imap'])
        config['smtp'] = iridium_section.get('smtp', DEFAULT_VALUES['smtp'])
        config['recipient'] = iridium_section.get('recipient', DEFAULT_VALUES['recipient'])
        config['password'] = iridium_section.get('password')

    elif 'port' in iridium_section:
        config['port'] = iridium_section.get('port')
        config['baud'] = iridium_section.get('baud')
        config['mailbox_check'] = iridium_section.getboolean('mailbox_check', DEFAULT_VALUES['mailbox_check'])
        config['discard_old'] = iridium_section.getboolean('discard_old', DEFAULT_VALUES['discard_old'])
        config['discard_gps'] = iridium_section.getboolean('discard_gps', DEFAULT_VALUES['discard_gps'])
        config['prepend_timestamp'] = iridium_section.getboolean('prepend_timestamp', DEFAULT_VALUES['prepend_timestamp'])
        config['discard_age'] = iridium_section.get('discard_age', DEFAULT_VALUES['discard_age'])
        config['strip_timestamp'] = iridium_section.getboolean('strip_timestamp', DEFAULT_VALUES['strip_timestamp'])
        config['loopback'] = iridium_section.getboolean('loopback', DEFAULT_VALUES['loopback'])
        config['xeos_cfg'] = iridium_section.get('xeos_cfg', DEFAULT_VALUES['xeos_cfg'])
    else:
        raise ValueError("Expected config option 'email' or 'port' in iridium section")

    config['beacon'] = iridium_section.get('beacon')
    config['unlock'] = iridium_section.get('unlock')
    config['interval'] = iridium_section.getint('interval', DEFAULT_VALUES['interval'])
    config['logdir'] = iridium_section.get('logdir', DEFAULT_VALUES['logdir'])

    return config


# Main entry point into the iridium driver.
#
# This gets installed as 'iridium_driver' into the system's $PATH by setup.py with
# `pip install -e .` from the repository root.
#
def main():
    import argparse

    parser = argparse.ArgumentParser(description=DESCRIPTION, prog="iridium_driver")

    # Mutually exclusive command line options:
    source = parser.add_mutually_exclusive_group(required=True)

    source.add_argument('--email', type=str, help="Email address to poll for new iridium messages")
    source.add_argument('--port', type=str, help="Iridium beacon serial port")
    source.add_argument('--config', type=str, help="Initialize daemon using a config file.")

    # Email related options
    email_group = parser.add_argument_group("Email Options")
    email_group.add_argument('--imap', type=str, metavar="address:port",
                             help="Incoming (IMAP) server address and port.", default=DEFAULT_VALUES['imap'])

    email_group.add_argument('--smtp', type=str, metavar="address:port",
                             help="Outgoing (SMTP) server address and port.", default=DEFAULT_VALUES['smtp'])

    email_group.add_argument('--recipient', type=str, help="Email address to send SBD payloads to.",
                             default=DEFAULT_VALUES['recipient'])

    email_group.add_argument('--password', type=str, help="Email account password.")

    # Serial port related options
    serial_group = parser.add_argument_group("Serial Port Options")
    serial_group.add_argument('--baud', type=int, help="Serial port baud rate", default=57600)

    # Beacon-to-beacon related options
    b2b_group = parser.add_argument_group("Beacon-to-Beacon Options")
    b2b_group.add_argument('--discard_old', type=bool, help="Discard all but newest message queued on Iridium gateway",
                           default=DEFAULT_VALUES['discard_old'])
    b2b_group.add_argument('--discard_gps', type=bool, help="Discard GPS fixes generated by some other beacon",
                           default=DEFAULT_VALUES['discard_gps'])
    b2b_group.add_argument('--prepend_timestamp', type=bool, help="Prepend a timestamp prior to uplinking messages",
                           default=DEFAULT_VALUES['prepend_timestamp'])
    b2b_group.add_argument('--discard_age', type=int, help="Discard by age in seconds.  Works if transmitting beacon is configured to prepend a timestamp.",
                           default=DEFAULT_VALUES['discard_age'])
    b2b_group.add_argument('--strip_timestamp', type=bool, help="Strips prepended timestamp if present.",
                           default=DEFAULT_VALUES['strip_timestamp'])
    b2b_group.add_argument('--loopback', type=bool, help="Loopback a message received over Iridium",
                           default=DEFAULT_VALUES['loopback'])

    # UDP options
    network_group = parser.add_argument_group("Network Options")
    network_group.add_argument("--listen", type=str, metavar="address:port", help="Local UDP address.",
                               default=DEFAULT_VALUES['listen'])

    network_group.add_argument("--remote", type=str, metavar="address:port", help="Remote UDP address",
                               default=DEFAULT_VALUES['remote'])

    # Iridium beacon options
    beacon_group = parser.add_argument_group("Iridium Beacon Options")
    beacon_group.add_argument("--beacon", type=str, help="Iridium beacon IMEI number")
    beacon_group.add_argument("--unlock", type=str, help="Iridium beacon unlock code")
    beacon_group.add_argument('--interval', type=int, help="Synchronous action interval (email or Iridium MT mailbox check)",
                             default=DEFAULT_VALUES['interval'])
    beacon_group.add_argument('--logdir', type=str, help="Logging directory",default=DEFAULT_VALUES['logdir'])
    beacon_group.add_argument('--xeos_cfg', type=str, help="Xeos configuration commands",default=DEFAULT_VALUES['xeos_cfg'])


    # Parse the command line arguments
    args = parser.parse_args()

    # Grab config parameters from a file?
    if args.config is not None:
        config = read_config_file(args.config)
        # Overwrite argument properties with those parsed from the config file
        for key, value in config.items():
            setattr(args, key, value)

    # Check for required arguments
    if args.beacon is None:
        raise ValueError("Missing beacon ID.  Provide it with --beacon or an entry in the config file.")
    if args.unlock is None:
        raise ValueError("Missing beacon unlock code.  Provide it with --unlock or an entry in the config file.")
    if args.email is not None and args.password is None:
        raise ValueError("Missing email account password.  Provide it with --password or an entry in the config file.")

    # Console and optionally logging to file
    logging.basicConfig(level=logging.DEBUG)
    if args.logdir:
        #fln = datetime.strftime(datetime.utcnow(),'%Y%m%d%H%M%S') + '.IRD'
        #flh = logging.FileHandler(filename=args.logdir + '/' + fln)
        flh = DslLogFileHandler(args.logdir, 'IRD')
        ffmt = logging.Formatter(fmt='%(asctime)s.%(msecs)03d %(levelname)s:%(message)s',datefmt='%Y-%m-%d %H:%M:%S')
        ffmt.converter = time.gmtime
        flh.setFormatter(ffmt)
        logging.getLogger('').addHandler(flh)

    # Get our event loop.
    loop = asyncio.get_event_loop()

    # Create the network link.
    (listen_addr, listen_port) = [x.strip() for x in args.listen.split(':')]
    (remote_addr, remote_port) = [x.strip() for x in args.remote.split(':')]

    (udp_transport, udp_protocol) = loop.run_until_complete(
        loop.create_datagram_endpoint(
            lambda: iridium.protocols.CommandProtocol(),
            local_addr=(listen_addr, int(listen_port))))
    sock = udp_transport.get_extra_info('socket')
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    # Create an iridium endpoint using xeos forward email service
    if args.email is not None:

        imap_server, imap_port = [x.strip() for x in args.imap.split(':')]
        smtp_server, smtp_port = [x.strip() for x in args.smtp.split(':')]

        (xeos_transport, xeos_protocol) = loop.run_until_complete(
            # iridium.transports.create_xeos_email_endpoint(
            iridium.transports.create_xeos_reconnecting_email_endpoint(
                lambda: iridium.protocols.XeosProtocol(),
                imap_addr=(imap_server, int(imap_port)),
                imap_login=(args.email, args.password),
                smtp_addr=(smtp_server, int(smtp_port)),
                smtp_login=(args.email, args.password),
                recipient=args.recipient))

        # Start polling when the loop starts up.
        xeos_transport.start_polling(args.interval)

    # Create an iridium endpoint using direct serial connection
    else:

        (xeos_transport, xeos_protocol) = loop.run_until_complete(
            iridium.transports.create_xeos_serial_endpoint(
                lambda: iridium.protocols.XeosProtocol(),
                args.port,args.baud))

        # Beacon configuration commands issued on startup.
        # A $FactoryDefaults command does exist but the beacon
        # takes some time to respond.  Would not work to issue that here.
        logging.getLogger('').info("Configuring beacon...")
        xeos_transport.sendto_beacon("","",'$cfgActive 1\r')
        xeos_transport.sendto_beacon("","",'$diag 1\r')
        for c in XEOS_CFG[args.xeos_cfg]:
            xeos_transport.sendto_beacon("","",c)
        xeos_transport.sendto_beacon("","",'$cfgActive 0\r')
        logging.getLogger('').info('Done configuring beacon.')

        # Start polling when the loop starts up.
        xeos_transport.start_polling(args.interval,args.mailbox_check)


    # now create the iridium service that glues everything together.
    service = iridium.services.IridiumService(
        xeos_transport=xeos_transport, xeos_protocol=xeos_protocol,
        udp_transport=udp_transport, udp_protocol=udp_protocol,
        forwarding_addr=(remote_addr, int(remote_port)),
        beacon=args.beacon, unlock=args.unlock, 
        discard_old=bool(args.discard_old), 
        discard_gps=bool(args.discard_gps),
        prepend_timestamp=bool(args.prepend_timestamp),
        discard_age=int(args.discard_age),
        strip_timestamp=bool(args.strip_timestamp),
        loopback=bool(args.loopback))

    # Start the event loop and block until keyboard interrupt is fired, then clean up.
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        print("\nCaught Ctrl-C")
    finally:
        # Stop polling
        udp_transport.close()
        xeos_transport.close()

        # Cancel all remaining pending tasks.
        pending = asyncio.Task.all_tasks()
        for task in pending:
            task.cancel()
            # Now we should await task to execute it's cancellation.
            # Cancelled task raises asyncio.CancelledError that we can suppress:
            with suppress(asyncio.CancelledError):
                loop.run_until_complete(task)
        # Stop the event loop.
        loop.stop()

    print("Done!")


if __name__ == '__main__':
    main()
