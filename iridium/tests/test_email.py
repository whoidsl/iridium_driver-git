import sys
import os

sys.path.insert(0, os.path.realpath(os.path.join(os.path.dirname(__file__), "../..")))

from iridium.transports import XeosEmailTransport


def test_generate_xeos_email():
    from_ = 'sentry.sat.coms@gmail.com'
    to = 'irid@xeostech.com'
    beacon = '23416641345213'
    unlock = '2346134'
    text = 'Some Text'
    payload_data = 'some data for the xeos'
    expected_payload = "$unlock %s\n$outPort A\n%s" % (unlock, payload_data)

    msg = XeosEmailTransport.create_xeos_sbd_email(
        from_=from_, beacon=beacon, unlock=unlock, payload=payload_data, text=text, addr=to)

    assert msg['From'] == from_
    assert msg['To'] == to
    assert msg['Subject'] == beacon
    assert msg.get_body().get_content().strip() == text

    attachments = list(msg.iter_attachments())
    assert len(attachments) == 1
    assert attachments[0].get_filename() == 'IRIDSBD.sbd'
    sbd = attachments[0].get_payload(decode=True)

    assert sbd == expected_payload.encode()
