# Need to insert the parent directory into the python path so we can load
# our module and actually do testing.
import sys
import os
import datetime

sys.path.insert(0, os.path.realpath(os.path.join(os.path.dirname(__file__), "../..")))
from iridium import parsing


def test_parse_xeos_fix_html():
    """
    Test parsing of HTML tables in 'Xeos Forward' emails
    """

    html = (
        '<table style="width:600px;"><tr><td style="font-weight:bold;">300434060425610</td></tr><tr><td><table style="width:100%;">'
        '<tr><td style="font-weight:bold;">Key</td><td style="font-weight:bold;">Value</td></tr><tr><td>Timestamp</td><td>2017-02-11T23:35:08.001Z</td></tr><tr>'
        '<td>Battery Level (V)</td><td>26.84</td></tr><tr><td>Latitude</td><td>-22.908896</td></tr><tr><td>Longitude</td><td>-112.018170</td></tr></table>'
        '</td></tr><tr><td style="padding-top:20px; font-weight:bold;">Reported GPS Positions</td></tr><tr><td><table style="width:100%;"><tr><td style="font-weight:bold;">'
        'Timestamp</td><td style="font-weight:bold;">Lat</td><td style="font-weight:bold;">Lon</td><td style="font-weight:bold;">Bearing</td>'
        '<td style="font-weight:bold;">Speed (km/h)</td><td style="font-weight:bold;">Altitude (m)</td><td style="font-weight:bold;">SNR</td></tr>'
        '<tr><td>2017-02-11T23:35:08.001Z</td><td>-22.908896</td><td>-112.018170</td><td>0.0</td><td>0.36000000000000004</td><td>0.0</td><td>48</td></tr>'
        '</table></td></tr><tr><td><div><img style="padding-top:20px" src="http://maps.googleapis.com/maps/api/staticmap?center=-22.908896,-112.0181696&'
        'zoom=16&size=500x300&maptype=roadmap&sensor=false&markers=color:red|-22.908896,-112.0181696" /></div></td></tr></table>'
    )

    expected = dict(
        lat=-22.908896,
        lon=-112.018170,
        battery=26.84,
        # timestamp='2017-02-11T23:35:08.001Z',
        timestamp=datetime.datetime(2017, 2, 11, 23, 35, 8, 1000),
        type='fix',
    )

    assert parsing.parse_xeos_fix_html(html) == expected


def test_parse_xeos_dev_html():
    """
    Test parsing a devdata email
    """

    html = (
        '<table style="width:600px;"><tr><td style="font-weight:bold;">300434060935010</td></tr><tr><td>'
        '<table style="width:100%;"><tr><td style="font-weight:bold;">Key</td><td style="font-weight:bold;">Value</td>'
        '</tr><tr><td>Timestamp</td><td>2016-08-05T14:19:31.000Z</td></tr><tr><td>Plain Text</td>'
        '<td>devData,a,1,1,1\ntheoareyouthere?\n</td></tr></table></td></tr></table>'
    )

    expected = dict(
        timestamp=datetime.datetime(2016, 8, 5, 14, 19, 31, 0),
        type='devdata',
        port='a',
        seq=1,
        part=1,
        total=1,
        payload=['theoareyouthere?']
    )

    assert parsing.parse_xeos_devdata_html(html) == expected


def test_parse_xeos_powerup_html():
    html = (
        '<table style="width:600px;"><tr><td style="font-weight:bold;">300434060935010</td></tr><tr><td>'
        '<table style="width:100%;"><tr><td style="font-weight:bold;">Key</td><td style="font-weight:bold;">Value</td>'
        '</tr><tr><td>Timestamp</td><td>2016-08-05T13:59:16.000Z</td></tr><tr><td>Powerup</td><td>true</td></tr>'
        '<tr><td>Firmware Version</td><td>XMI-Relay v1.25-2842. dev:4 </td></tr><tr><td>Hardware Revision</td>'
        '<td>2.6-1</td></tr><tr><td>Serial Number</td><td>131</td></tr><tr><td>GPS Version</td>'
        '<td>GSD4e_4.1.2-P1 R+ 11/15/2011 319-Nov 15 </td></tr><tr><td>Iridium Version</td><td>TA13001</td></tr><tr>'
        '<td>Reset Data</td><td> Count=15, Current=(P), Prev=(cleared)</td></tr></table></td></tr></table>'
    )

    expected = dict(
        type='powerup',
        timestamp=datetime.datetime(2016, 8, 5, 13, 59, 16, 0),
        firmware='XMI-Relay v1.25-2842. dev:4',
        hardware_rev='2.6-1',
        serial=131,
        gps_ver='GSD4e_4.1.2-P1 R+ 11/15/2011 319-Nov 15',
        iridium_ver='TA13001',
        reset_data='Count=15, Current=(P), Prev=(cleared)'
    )

    assert parsing.parse_xeos_powerup_html(html) == expected


def test_xeos_fix_to_gga():
    fix_dict = {
        'battery': 26.84,
        'lon': -111.634432,
        'timestamp': datetime.datetime(2017, 2, 6, 17, 6, 39, 1000),
        'type': 'fix',
        'lat': -23.344506,
        'beacon': '300434060425610'}

    expected = "$GPGGA,170639,2320.67036,S,11138.06592,W,1,05,1.5,0.0,M,0.0,M,,,*7B\r\n"

    assert parsing.xeos_fix_dict_to_gga(fix_dict) == expected
