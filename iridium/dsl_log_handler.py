try:
    import codecs
except ImportError:
    codecs = None

import logging.handlers
import time
import os
import datetime
import calendar

class DslLogFileHandler(logging.handlers.TimedRotatingFileHandler):
    """
    Implements DSL-style log file handling.  Rotates the log file at the top of the hour and 
    sets the file name to a DSL-correct YYYYMMDD_HHMM.<type> format.  Also makes sure
    the destination directory exists
    """

    def __init__(self, log_dir, log_type):
        self.log_dir = log_dir
        self.log_type = log_type

        # Make sure we have an output directory
        if not os.path.isdir(log_dir):
            os.makedirs(log_dir)

        # Compute the filename
        n = datetime.datetime.utcnow()
        log_fname = self.getFilename(n)

        # Initialize logging using our superclass
        logging.handlers.TimedRotatingFileHandler.__init__(self,
                                                           log_fname,
                                                           when='H',
                                                           interval=10,
                                                           backupCount=0,
                                                           encoding=None)
        
    # Override this from the parent class
    def computeRollover(self, t):
        # Find the top of the current hour
        now = datetime.datetime.utcfromtimestamp(t)
        topOfHour = datetime.datetime(now.year, now.month, now.day, now.hour, 0, 0)

        # Then advance 1 hour
        return calendar.timegm(topOfHour.timetuple()) + 3600

    def getFilename(self, n):
        log_fname = os.path.join(self.log_dir, n.strftime("%Y%m%d_%H%M") + '.' + self.log_type)

        return os.path.abspath(log_fname)


    def doRollover(self):
        """
        TimedRotatingFileHandler remix - rotates logs at the top of the hour and sets log name correctly
        """
        self.stream.close()
        n = datetime.datetime.utcnow()
        t = calendar.timegm(n.timetuple())
        self.baseFilename = self.getFilename(n)
        self.rolloverAt = self.computeRollover(t)

        if self.encoding:
            self.stream = codecs.open(self.baseFilename, 'w', self.encoding)
        else:
            self.stream = open(self.baseFilename, 'w')



