###
# Message parsing functions should go here.
#
#
import re
from datetime import datetime
from datetime import timedelta
import email

from bs4 import BeautifulSoup

import bitstring

def parse_xeos_email(xeos_email: email.message.EmailMessage) -> dict:
    r"""

    @param xeos_email:
    @return:
    """

    # We're only interested in emails from xeostech
    if xeos_email['From'] != 'online@online.xeostech.com':
        return

    # Xeos Forward messages look like:
    #   'Xeos Forward - 300434060935010'
    (preamble, sep, beacon) = xeos_email['Subject'].partition('-')
    if sep == '':
        return
    elif preamble.strip() != 'Xeos Forward':
        return

    # Strip whitespace from beacon id.
    beacon = beacon.strip()

    # Get the payload of the email.  It's html.
    email_html = xeos_email.get_payload(0).get_payload()

    # Some types of emails get wrapped by '=\n' markers.
    email_html = email_html.replace('=\r\n', '')

    # Three types of emails:  powerup, beacon fix, and custom data
    if 'devData' in email_html:
        result = parse_xeos_devdata_html(email_html)

    elif 'Powerup' in email_html:
        result = parse_xeos_powerup_html(email_html)

    elif 'Reported GPS' in email_html:
        result = parse_xeos_fix_html(email_html)

    else:
        return None

    result['beacon'] = beacon
    return result


def parse_xeos_devdata_html(html_msg: str) -> dict:
    r"""
    Parse a 'Mobile Originated' Xeos message.
    @param html_msg:
    @return: dict

    Returned dict has the following:
        Key         Type                       Desc
        type        str                        Set to 'fix'
        timestamp   class:`datetime.datetime`  Timestamp of fix
        port        str                        Iridium port that received message
        seq         int                        Message sequence number
        part        int                        This message part
        total       int                        Total parts in this message
        payload     [str]                      Data sent from remote iridium
    """

    soup = BeautifulSoup(html_msg, 'html.parser')

    timestamp = soup.find('td', text='Timestamp')
    if timestamp is None:
        raise ValueError("Could not find timestamp element")

    timestamp = datetime.strptime(timestamp.next_sibling.text, '%Y-%m-%dT%H:%M:%S.%fZ')

    dev_data = soup.find('td', text=re.compile(r'^devData.*'))
    if dev_data is None:
        raise ValueError("Could not find devData element")

    payload = dev_data.text.splitlines()
    header, payload = payload[0], payload[1:]
    _, port, seq, part, total = header.split(',')
    return dict(timestamp=timestamp, type='devdata', port=port, seq=int(seq), part=int(part), total=int(total),
                payload=payload)


def parse_xeos_powerup_html(html_msg) -> dict:
    r"""

    @param html_msg:
    @return: dict

    Returned dictionary will have the following keys:

        Key          Type                       Desc
        type         str                        Set to 'powerup'
        timestamp    class:`datetime.datetime`  Timestamp of fix
        hardware_rev str                        Hardware revision
        serial       int                        Serial Number
        gps_ver      str                        GPS version
        iridium_ver  str                        Iridium version
        reset_data   str                        Reset data

    """

    soup = BeautifulSoup(html_msg, 'html.parser')

    strip = lambda s: s.strip()

    tags = (
        ('Timestamp', lambda t: datetime.strptime(t, '%Y-%m-%dT%H:%M:%S.%fZ'), 'timestamp'),
        ('Firmware Version', strip, 'firmware'),
        ('Hardware Revision', strip, 'hardware_rev'),
        ('Serial Number', int, 'serial'),
        ('GPS Version', strip, 'gps_ver'),
        ('Iridium Version', strip, 'iridium_ver'),
        ('Reset Data', strip, 'reset_data'),
    )

    msg = generic_table_parser(soup, tags)
    msg['type'] = 'powerup'

    return msg


def parse_xeos_fix_html(html_msg):
    """
    Parse a Xeos iridium fix HTML table.

    @param html_msg: HTML payload from Xeos Forward email.
    @type html_msg: str

    @return: dict

    @raise: ValueError if unable to find required data.

    Returned dictionary will have the following keys:

        Key         Type                       Desc
        type        str                        Set to 'fix'
        beacon_id:  str                        ID of Xeos beacon
        timestamp   class:`datetime.datetime`  Timestamp of fix
        battery     float                      Battery level of beacon
        lat         float                      Fix latitude (+N)
        lon         float                      Fix longitude (+E)

    """

    # Parse the HTML
    soup = BeautifulSoup(html_msg, 'html.parser')

    tags = (
        ('Timestamp', lambda t: datetime.strptime(t, '%Y-%m-%dT%H:%M:%S.%fZ'), 'timestamp'),
        (re.compile(r'.*Battery.*'), float, 'battery'),
        ('Latitude', float, 'lat'),
        ('Longitude', float, 'lon'),
    )

    msg = generic_table_parser(soup, tags)
    msg['type'] = 'fix'

    return msg


def parse_xeos_fix_binary(hex_msg):
    """
    Parse a Xeos iridium fix in binary Xeos format used for SBD.

    @param bin_msg: HTML payload from Xeos Forward email.
    @type bin_msg: str

    @return: dict

    @raise: ValueError if unable to find required data.

    Returned dictionary will have the following keys:

        Key         Type                       Desc
        type        str                        Set to 'fix'
        beacon_id:  str                        ID of Xeos beacon
        timestamp   class:`datetime.datetime`  Timestamp of fix
        battery     float                      Battery level of beacon
        lat         float                      Fix latitude (+N)
        lon         float                      Fix longitude (+E)

    """

    msg = dict()

    b = bitstring.BitStream('').join([bitstring.pack('hex',s) for s in hex_msg.split()])
    start_char = b.read('hex:8')
    msg_id = b.read('hex:8')
    msg['battery'] = b.read('uint:12')/100.0 # V
    payload_len = b.read('uint:12')
    hdr_chksum = b.read('uint:8')
    
    tsec = b.read('uint:32') # seconds since jan 1, 2000
    msg['lat'] = b.read('int:26')*64/1e7
    max_snr = b.read('uint:6')
    msg['lon'] = b.read('int:26')*64/1e7
    latLonRelativeMode = b.read('bin:1')  # should always be 0 for first fix in message.
    spare_bit = b.read('bin:1')
    sat_qty = b.read('uint:4')
    gps_sec_on = b.read('uint:8')
    hdop = b.read('uint:8')/10.0
    speed = b.read('uint:8')/10.0 # m/s
    heading = b.read('uint:7')*3.0 # deg
    
    # @@@ additional fixes can appear appended to this.  They have a different format.
    # @@@ would need to return and parse again with previous fix and rel mode as arguments.

    ts0 = datetime( year=2000, month=1, day=1, hour=0, minute=0, second=0) 
    dt = timedelta(seconds = tsec)
    msg['timestamp'] = ts0 + dt

    msg['beacon_id'] = None # not contained in message
    msg['type'] = 'fix'

    return msg

def generic_table_parser(soup, parsing_rules) -> dict:
    r"""

    @param soup:
    @param parsing_rules:
    @return:

    Somewhat generic method of pulling records from html table.

    `parsing_rules` is a list of 3-tuples:
        (search_term, conversion function, message_key)

     search_term:  Can be a string or a compiled regular function.
                   Searches 'td' elements in the formatted table

     conversion_func:  Function to run on the desired value.  If None
                       no conversion is done

     message_key:  Key in the returned dictionary to store the value under
    """

    msg = {}

    # Start getting values
    for (tag_search, conv_func, key) in parsing_rules:

        # Find the <td> element matching our search term
        elem = soup.table.find('td', text=tag_search)

        # Couldn't find one?  Raise a ValueError
        if elem is None:
            if isinstance(tag_search, str):
                raise ValueError("Unable to find plain-text entry: %r" % (tag_search,))
            else:
                raise ValueError("Unable to find regex pattern: %r" % (tag_search.pattern,))

        # The value is in the *NEXT* <td> element.
        value = elem.next_sibling.text

        # Run any conversion function
        if conv_func is not None:
            value = conv_func(value)

        msg[key] = value

    return msg


def xeos_fix_dict_to_gga(xeos_fix) -> str:
    r"""
    Make a GPGGA string from data parsed from a Xeos fix email.

    @param xeos_fix:
    @type xeos_fix: dict
    @return:

    `xeos_fix` has the following:

        Key         Type                       Desc
        type        str                        Set to 'fix'
        beacon_id:  str                        ID of Xeos beacon
        timestamp   class:`datetime.datetime`  Timestamp of fix
        battery     float                      Battery level of beacon
        lat         float                      Fix latitude (+N)
        lon         float                      Fix longitude (+E)

    """

    timestr = xeos_fix['timestamp'].strftime('%H%M%S')
    if xeos_fix['lat'] < 0:
        lat_dir = 'S'
    else:
        lat_dir = 'N'

    lat = abs(xeos_fix['lat'])
    lat_degrees = int(abs(lat))
    lat_minutes = (lat - lat_degrees) * 60.0

    if xeos_fix['lon'] < 0:
        lon_dir = 'W'
    else:
        lon_dir = 'E'

    lon = abs(xeos_fix['lon'])
    lon_degrees = int(abs(lon))
    lon_minutes = (lon - lon_degrees) * 60.0

    msg = "$GPGGA,{time:s},{lat:04.5f},{lat_dir:s},{lon:05.5f},{lon_dir:s},1,05,1.5,0.0,M,0.0,M,,,".format(
        time=timestr, lat=lat_degrees * 100 + lat_minutes, lat_dir=lat_dir, lon=lon_degrees * 100 + lon_minutes,
        lon_dir=lon_dir)

    checksum = 0
    for x in msg[1:]:  # Leading '$' is not included in NMEA checksum
        checksum ^= ord(x)

    return msg + "*%02X\r\n" % (checksum,)

