r"""
Xeos Iridium communication transports.

Interface classes for dealing with iridium messages over various
transports

-  EmailTransport:  IMAP/SMTP interface for Xeos iridium
"""
import asyncio
import logging
from concurrent.futures import ThreadPoolExecutor
import weakref
from collections import deque

import imaplib
import smtplib

import email
from email.message import EmailMessage

import functools

from serial import Serial

from datetime import datetime
from datetime import timedelta

from iridium.protocols import XeosProtocol


class XeosBaseTransport(asyncio.BaseTransport):
    r"""
    Base transport class for Xeos iridiums.

    Should not be used as is -- it only serves as a useful
    template for describing methods common to the derived transports.
    """

    def sendto_beacon(self, beacon: str, unlock: str, payload: str) -> asyncio.Task:
        r"""
        Send an message to an iridium beacon.

        @param beacon: Beacon ID
        @type beacon: str

        @param unlock: Unlock code
        @type unlock: str

        @param payload:  Message payload
        @type payload: str

        @return: `asnycio.Task`
        """
        NotImplemented


class XeosEmailTransport(XeosBaseTransport):
    r"""
    Send and receive iridium messages via email through the "Xeos Forward" service.

    Useful methods:

        `connect`: Connect to the IMAP and SMTP servers (coroutine)
        `start_polling`: Start polling the IMAP server for new email messages
        `stop_polling`: Stop polling the IMAP server.
        `sendto_beacon`: Send data to a remote iridium beacon through email.
    """

    def __init__(self, imap_addr, imap_login, smtp_addr, smtp_login, recipient='data@sbd.iridium.com'):
        r"""

        @param imap_addr:  IMAP server and port (incoming emails)
        @type imap_addr: (str, int)

        @param imap_login: Login and password for IMAP server
        @type imap_login: (str, str)

        @param smtp_addr: SMTP server and port (outgoing emails)
        @type smtp_addr: (str, int)

        @param smtp_login: Login and password for SMTP server
        @type smtp_login: (str, str)

        @param recipient: Receiving address
        @type recipient: str
        """

        super(XeosEmailTransport, self).__init__(
            extra=dict(
                imap_addr=imap_addr, imap_login=imap_login, smtp_addr=smtp_addr, smtp_login=smtp_login,
                recipient=recipient))

        self.imap_addr = imap_addr
        self.imap_login = imap_login

        self.imap = None

        self.smtp_addr = smtp_addr
        self.smtp_login = smtp_login
        self.smtp = None

        self.recipient = recipient

        self.protocol = None

        # Threadpool Executor for our blocking I/O
        # TODO:  Set the pool size to something reasonable
        self._threadpool = ThreadPoolExecutor(3)

        # Message id's retrieved so far
        self._msg_ids = None

        # Our logging instance
        self._log = logging.getLogger(__name__ + "." + __class__.__name__)

        self.__poll_future = None

        self.__is_closing = True

    @asyncio.coroutine
    def connect(self, protocol: XeosProtocol):
        r"""
        Connect to the IMAP and SMTP servers

        @param protocol: Xeos protocol
        @type protocol: `protocol.XeosProtocol`

        Received email messages will be forwarded to the `message_received` method of
        the attached `protocol.XeosProtocol` class.

        @return:
        """

        # Connect to IMAP and SMTP
        yield from self._connect_imap()
        yield from self._connect_smtp()

        # Connected!  Save a weakref to our protocol
        self.protocol = weakref.ref(protocol)

    def close(self):
        r"""
        Close the email connection
        @return:  None
        """

        self.__is_closing = True
        self.stop_polling()

        # Close the connections async.
        asyncio.ensure_future(self._disconnect_imap())
        asyncio.ensure_future(self._disconnect_smtp())

    def is_closing(self):
        r"""
        Return True if the transport is closing or is closed.
        @return: bool
        """

        return self.__is_closing

    def sendto_beacon(self, beacon: str, unlock: str, payload: str) -> asyncio.Task:
        r"""
        Sends data to a Xeos Iridium beacon through the Xeos Forward email service.

        @param beacon: Beacon ID
        @type beacon: str

        @param unlock: Beacon unlock code
        @type unlock: str

        @param payload: Data to send
        @type payload: str

        @return: `asyncio.Task`

        `data` is packed into a binary sbd attachment.
        """

        if self.smtp is None:
            raise RuntimeError("You must log into a SMTP server before sending emails")

        email_msg = self.create_xeos_sbd_email(
            from_='sentry.sat.coms@gmail.com', beacon=beacon, unlock=unlock, payload=payload, addr=self.recipient)

        self._log.debug("New outgoing email: %s", str(email_msg))

        loop = asyncio.get_event_loop()

        return asyncio.ensure_future(
            loop.run_in_executor(self._threadpool, functools.partial(self.smtp.send_message, email_msg)))

    @staticmethod
    def create_xeos_sbd_email(from_: str, beacon: str, unlock: str, payload: str, text=None,
                              addr='data@sbd.iridium.com') -> EmailMessage:
        r"""
        Create an email message to send to an Iridium using the Xeos Forward service.

        @param from_:  The sender's email address
        @type from_: str

        @param beacon: Beacon ID
        @type beacon: str

        @param unlock: Unlock code
        @type unlock: str

        @param payload: Payload data for Iridium
        @type payload: str

        @param text: Optional Email text
        @type text: str

        @param addr: Recepient's email address
        @type addr: str

        @return: EmailMessage
        """

        msg = EmailMessage()
        msg['From'] = from_
        msg['To'] = addr
        msg['Subject'] = beacon

        if text is not None:
            msg.set_content(text)

        sbd = "$unlock %s\n$outPort A\n%s" % (unlock, payload)
        msg.add_attachment(sbd.encode(), 'application', 'octet-stream', filename='IRIDSBD.sbd')

        return msg

    @asyncio.coroutine
    def _connect_imap(self):
        r"""
        Connect to the IMAP server using the provided address and credentials.
        """

        # Already connected?  Don't do anything
        if self.imap is not None:
            return

        # Grab the event loop
        loop = asyncio.get_event_loop()

        # Open the connection
        self._log.info("Opening connection to %s:%d", self.imap_addr[0], self.imap_addr[1])
        imap = yield from loop.run_in_executor(self._threadpool, imaplib.IMAP4_SSL, self.imap_addr[0],
                                               self.imap_addr[1])

        # Login
        self._log.info("Logging in with username %s", self.imap_login[0])
        yield from loop.run_in_executor(self._threadpool,
                                        functools.partial(imap.login, self.imap_login[0], self.imap_login[1]))

        self.imap = imap
        self._log.info("Connected.")

    @asyncio.coroutine
    def _disconnect_imap(self):
        r"""
        Disconnect from the IMAP server.
        @return:
        """

        # Not connected? Don't do anything
        if self.imap is None:
            return

        # Grab the event loop
        loop = asyncio.get_event_loop()

        self._log.info("Shutting down connection to %s", self.imap_addr[0])

        try:
            yield from loop.run_in_executor(self._threadpool, self.imap.shutdown)
        except:
            self._log.exception("Unable to close IMAP connection")

        _, self.imap = self.imap, None

    @asyncio.coroutine
    def _connect_smtp(self):
        r"""
        Connect to the SMTP server using the provided address and credentials.
        """

        # Already connected?  Don't do anything
        if self.smtp is not None:
            return

        # Grab the event loop
        loop = asyncio.get_event_loop()

        # Open the connection
        self._log.info("Opening connection to %s:%d", self.smtp_addr[0], self.smtp_addr[1])
        smtp = yield from loop.run_in_executor(self._threadpool, smtplib.SMTP, self.smtp_addr[0], self.smtp_addr[1])
        smtp.ehlo()
        smtp.starttls()

        # Login
        self._log.info("Logging in with username %s", self.smtp_login[0])
        yield from loop.run_in_executor(self._threadpool,
                                        functools.partial(smtp.login, self.smtp_login[0], self.smtp_login[1]))

        self.smtp = smtp
        self._log.info("Connected.")

    @asyncio.coroutine
    def _disconnect_smtp(self):
        r"""
        Disconnect from the smtp server.
        """

        # Not connected? Don't do anything
        if self.smtp is None:
            return

        # Grab the event loop
        loop = asyncio.get_event_loop()

        self._log.info("Shutting down connection to %s", self.smtp_addr[0])
        try:
            yield from loop.run_in_executor(self._threadpool, self.smtp.quit)
        except:
            self._log.exception("Unable to disconnect from SMTP server")

        _, self.smtp = self.smtp, None

    @asyncio.coroutine
    def _check_imap(self):
        r"""
        Check for new incoming iridium emails.

        `self.protocol.message_received` is called for each new email message.
        """

        # Not connected?  Need to do that first.
        if self.imap is None:
            yield from self._connect_imap()

        # Grab the event loop
        loop = asyncio.get_event_loop()

        # Select the mailbox, set readonly
        self._log.debug("Selecting IMAP folder: %s", 'INBOX')
        yield from loop.run_in_executor(self._threadpool, functools.partial(self.imap.select, 'INBOX', readonly=True))

        # Time to get the emails.  If we haven't before, get ALL the emails.
        if self._msg_ids is None:
            self._msg_ids = set([])
            tmin = datetime.strftime(datetime.utcnow()-timedelta(days=5),'%d-%b-%Y')
            self._log.info("Retrieving message ids newer than %s for IMAP folder: %s", tmin, 'INBOX')
            search = 'SINCE ' + tmin
        else:
            tmin = datetime.strftime(datetime.utcnow()-timedelta(days=1),'%d-%b-%Y')
            self._log.info("Retrieving message ids newer than %s for IMAP folder: %s", tmin, 'INBOX')
            search = 'SINCE ' + tmin

        ret_code, msg_ids = yield from loop.run_in_executor(self._threadpool,
                                                            functools.partial(self.imap.search, None, search))

        if ret_code != 'OK':
            self._log.warning("Searching %s returned: %r", 'INBOX', ret_code)

        msg_ids = msg_ids[0]

        msg_ids = set([int(x) for x in msg_ids.split()]) - self._msg_ids
        num_msgs = len(msg_ids)

        self._log.info("Found %d new messages", len(msg_ids))

        # No messages?  We're done.  Close the folder and quit.
        if num_msgs == 0:
            # Close the folder
            yield from loop.run_in_executor(self._threadpool, self.imap.close)
            return

        # Start pulling emails until we find a beacon.  Go in order so that we
        # pull from oldest->newest
        for idx, _id in enumerate(sorted(list(msg_ids), reverse=False)):
            self._log.debug("Fetching message id: %d (%d of %d)", _id, idx + 1, num_msgs)
            ret_code, results = yield from loop.run_in_executor(self._threadpool,
                                                                functools.partial(self.imap.fetch, str(_id),
                                                                                  '(RFC822)'))
            if len(results) == 0:
                self._log.warning("Fetch succeeded but yielded no message for id: %d.", _id)
                continue

            # Get at the message text
            _, email_bytes = results[0]

            # Decode it
            email_msg = email.message_from_string(email_bytes.decode())

            # Handler attached?
            if self.protocol is not None:
                # And still available?
                protocol = self.protocol()
                # Then act on the new email
                # TODO:  Allow yielding to handler?  If handler is a coroutine or task?
                if protocol is not None:
                    try:
                        protocol.message_received(email_msg)
                    except:
                        self._log.exception("Unable to process email: %r", str(email_msg))

            # Update our processed list of message ids
            self._msg_ids = set([_id]) | self._msg_ids

        # Close the folder
        yield from loop.run_in_executor(self._threadpool, self.imap.close)

    @asyncio.coroutine
    def _poll_imap(self, interval=180):
        r"""
        Private method to poll for incoming messages.

        @param interval:  Interval to poll for new messages (seconds)
        @type interval: int

        Not intended for use by user.  Use `start()`
        """

        if self.imap_addr is None:
            raise RuntimeError("Set the IMAP address before polling.")

        if self.imap_login is None:
            raise RuntimeError("Set the IMAP login and password before polling.")

        # Loop to check incoming messages.  Don't raise the CancelledError
        while True:
            try:
                yield from self._check_imap()
            except asyncio.CancelledError:
                break
            except imaplib.IMAP4.error as e:
                self._log.error("IMAP error: %s", str(e))
            except:
                self._log.exception()

            self._log.debug("Waiting %d seconds before next incoming check", interval)
            try:
                yield from asyncio.sleep(interval)
            except asyncio.CancelledError:
                break

    def start_polling(self, interval=180):
        r"""
        Start polling for new email messages.

        The IMAP address (IP and port) and the account credentials (username and password)
        must be set before calling start.

        To stop polling, call the `stop` function.
        """
        if self.__poll_future is not None:
            raise RuntimeError("Polling already started.")

        self.__poll_future = asyncio.ensure_future(self._poll_imap(interval))

    def stop_polling(self):
        r"""
        Cancel all pending operations.
        @return:
        """

        if self.__poll_future is not None:
            self.__poll_future.cancel()
            self.__poll_future = None


@asyncio.coroutine
def create_xeos_email_endpoint(
        protocol_factory, imap_addr: (str, int), imap_login: (str, str), smtp_addr: (str, int),
        smtp_login: (str, str), recipient='data@sbd.iridium.com'):
    r"""
    Creates an Email endpoint for use with Xeos Iridium Beacons.

    @param protocol_factory:  Callable returning a protocol instance

    @param imap_addr: IMAP server address and port
    @type imap_addr: (str, int)

    @param imap_login: IMAP login and password
    @type imap_login: (str, str)

    @param smtp_addr: SMTP address and port
    @type smtp_addr:  (str, int)

    @param smtp_login: SMTP login and password
    @type smtp_login: (str, str)

    @param recipient: Recepient of emails
    @type recipient: str

    @return: (XeosEmailTransport, protocol)
    """

    # Create the transport and protocol
    transport = XeosEmailTransport(
        imap_addr=imap_addr, imap_login=imap_login, smtp_addr=smtp_addr, smtp_login=smtp_login, recipient=recipient)
    protocol = protocol_factory()

    # Connect the transport up to the email servers
    yield from transport.connect(protocol=protocol)

    # Attach the transport to the protocol
    protocol.connection_made(transport)

    return (transport, protocol)


class XeosReconnectingEmailTransport(XeosEmailTransport):
    r"""
    Send and receive iridium messages via email through the "Xeos Forward" service.

    This differs from 'XeosEmailTransport' in that it attempts to connect/disconnect
    from the IMAP server before every poll or email sent.

    Useful methods:

        `connect`: Connect to the IMAP and SMTP servers (coroutine)
        `start_polling`: Start polling the IMAP server for new email messages
        `stop_polling`: Stop polling the IMAP server.
        `sendto_beacon`: Send data to a remote iridium beacon through email.
    """

    def __init__(self, imap_addr, imap_login, smtp_addr, smtp_login, recipient='data@sbd.iridium.com'):
        r"""

        @param imap_addr:  IMAP server and port (incoming emails)
        @type imap_addr: (str, int)

        @param imap_login: Login and password for IMAP server
        @type imap_login: (str, str)

        @param smtp_addr: SMTP server and port (outgoing emails)
        @type smtp_addr: (str, int)

        @param smtp_login: Login and password for SMTP server
        @type smtp_login: (str, str)

        @param recipient: Receiving address
        @type recipient: str
        """

        super(XeosReconnectingEmailTransport, self).__init__(
            imap_addr=imap_addr, imap_login=imap_login, smtp_addr=smtp_addr, smtp_login=smtp_login,
            recipient=recipient)

    @asyncio.coroutine
    def connect(self, protocol: XeosProtocol):
        r"""
        Connect to the IMAP and SMTP servers

        @param protocol: Xeos protocol
        @type protocol: `protocol.XeosProtocol`

        Received email messages will be forwarded to the `message_received` method of
        the attached `protocol.XeosProtocol` class.

        @return:
        """

        # Connect to IMAP and SMTP
        # yield from self._connect_imap()
        # yield from self._connect_smtp()

        # Connected!  Save a weakref to our protocol
        self.protocol = weakref.ref(protocol)

    @asyncio.coroutine
    def _check_imap(self):
        r"""
        Check for new incoming iridium emails.

        `self.protocol.message_received` is called for each new email message.
        """

        # Not connected?  Need to do that first.
        if self.imap is None:
            yield from self._connect_imap()

        # Do the usual checking.
        yield from super(XeosReconnectingEmailTransport, self)._check_imap()

        # Disconnect
        yield from self._disconnect_imap()

    @asyncio.coroutine
    def _sendto_beacon(self, beacon: str, unlock: str, payload: str):
        r"""
        Sends data to a Xeos Iridium beacon through the Xeos Forward email service.

        @param beacon: Beacon ID
        @type beacon: str

        @param unlock: Beacon unlock code
        @type unlock: str

        @param payload: Data to send
        @type payload: str

        @return: `asyncio.Task`

        `data` is packed into a binary sbd attachment.
        """
        if self.smtp is None:
            yield from self._connect_smtp()

        email_msg = self.create_xeos_sbd_email(
            from_='sentry.sat.coms@gmail.com', beacon=beacon, unlock=unlock, payload=payload, addr=self.recipient)

        self._log.debug("New outgoing email: %s", str(email_msg))

        loop = asyncio.get_event_loop()

        # Send the message
        yield from asyncio.ensure_future(
            loop.run_in_executor(self._threadpool, functools.partial(self.smtp.send_message, email_msg)))

        # Disconnect
        yield from self._disconnect_smtp()

    def sendto_beacon(self, beacon: str, unlock: str, payload: str) -> asyncio.Task:
        r"""
        Sends data to a Xeos Iridium beacon through the Xeos Forward email service.

        @param beacon: Beacon ID
        @type beacon: str

        @param unlock: Beacon unlock code
        @type unlock: str

        @param payload: Data to send
        @type payload: str

        @return: `asyncio.Task`

        `data` is packed into a binary sbd attachment.
        """

        loop = asyncio.get_event_loop()

        # TODO Timeouts
        asyncio.ensure_future(self._sendto_beacon(beacon, unlock, payload))

@asyncio.coroutine
def create_xeos_reconnecting_email_endpoint(
        protocol_factory, imap_addr: (str, int), imap_login: (str, str), smtp_addr: (str, int),
        smtp_login: (str, str), recipient='data@sbd.iridium.com'):
    r"""
    Creates an Email endpoint for use with Xeos Iridium Beacons.

    @param protocol_factory:  Callable returning a protocol instance

    @param imap_addr: IMAP server address and port
    @type imap_addr: (str, int)

    @param imap_login: IMAP login and password
    @type imap_login: (str, str)

    @param smtp_addr: SMTP address and port
    @type smtp_addr:  (str, int)

    @param smtp_login: SMTP login and password
    @type smtp_login: (str, str)

    @param recipient: Recepient of emails
    @type recipient: str

    @return: (XeosEmailTransport, protocol)
    """

    # Create the transport and protocol
    transport = XeosReconnectingEmailTransport(
        imap_addr=imap_addr, imap_login=imap_login, smtp_addr=smtp_addr, smtp_login=smtp_login, recipient=recipient)
    protocol = protocol_factory()

    # Connect the transport up to the email servers
    yield from transport.connect(protocol=protocol)

    # Attach the transport to the protocol
    protocol.connection_made(transport)

    return (transport, protocol)


class XeosSerialTransport(XeosBaseTransport):
    r"""
    Send and receive iridium messages via email.

    Useful methods:
        `connect`:  Open the serial port
        `sendto_beacon`: Send data to the attached iridium beacon
    """

    def __init__(self, serialport=Serial):
        r"""

        @param serialport:  Serial port connected to Iridium
        @type serialport: class::`Serial`
        """

        super(XeosSerialTransport, self).__init__(
            extra=dict(serial=serialport))

        self.serial = serialport

        self.protocol = None

        # Threadpool Executor for our blocking I/O
        # TODO:  Set the pool size to something reasonable somehow
        self._threadpool = ThreadPoolExecutor(3)

        self._log = logging.getLogger(__name__ + "." + __class__.__name__)

        self.__is_closing = True

        self.__output_buffer = deque()

        self.__poll_future = None

        # Some actions depend on whether beacon reports it is submerged or on the surface.
        self.beacon_status = None

        # Skip the next internally-generated mailbox-check/uplink
        self.skip_sync_flag = False

    @asyncio.coroutine
    def connect(self, protocol: XeosProtocol):
        r"""
        Connect (open) the serial port
        @return:
        """

        if not self.serial.isOpen():
            self.serial.open()

        # Timeout of 1 sec works ok.  Buffering and readline-like 
        # behavior would break multi-line message interpretation elsewhere.
        self.serial.timeout = 1

        # Add a reader
        loop = asyncio.get_event_loop()
        loop.add_reader(self.serial.fileno(), self.__read_serial)

        # Connected!  Save a weakref to our protocol
        self.protocol = weakref.ref(protocol)


    def close(self):
        r"""
        Close the email connection
        @return:  None
        """

        self.__is_closing = True
        # Add a reader
        loop = asyncio.get_event_loop()

        # Remove our hook
        loop.remove_reader(self.serial.fileno())

        # stop polling
        self.stop_polling()

        # Close the serial port
        self.serial.close()

        # Inform the protocol
        self.protocol.connection_lost(None)

    def is_closing(self):
        r"""
        REturn True if the transport is closing or is closed.
        @return: bool
        """

        return self.__is_closing

    def set_beacon_status(self, beacon_status: str):
        r"""
        Set beacon status (surfaced, submerged, etc.)

        @param: beacon_status: Beacon status string
        @type: str

        Beacon status determines whether MT mailbox checks are attempted.
        """

        self.beacon_status = beacon_status

    def sendto_beacon(self, beacon: str, unlock: str, payload: str) -> asyncio.Task:
        r"""
        Sends data to a Xeos Iridium beacon through its diagnostic port (serial).

        @param beacon: Beacon ID
        @type beacon: str

        @param unlock: Beacon unlock code
        @type unlock: str

        @param payload: Data to send
        @type payload: str

        @return: `asyncio.Task`

        `data` is packed into a binary sbd attachment.
        """

        if self.serial.closed:
            raise RuntimeError("The serial port is closed")


        # Interpret messages prefixed with a '$' as intended for interacting with
        # the beacon itself.  This is mostly for debugging purposes.
        if payload[0] == '$':
            self._log.info("Sending as diagnostic command to beacon: %r", payload)

        elif self.beacon_status in ['NORMAL','STARTUP','SURFACED']:
            # incoming UDP JT gets uplinked via Iridium.  Add 
            # the Xeos protocol.  Ensure a CR appears at the end.
            # Could use $msfClear but small buffers might be useful.  $msfMM=1
            # achieves the same thing and is set at startup.
            self._log.info("Sending for Iridium uplink: %r", payload)
            payload = '$sendSBD\r' + payload.rstrip() + '\r' + '$finished\r'

            # Skip the next synchronous mailbox check if enabled.
            self.skip_next_sync = True

        else:
            self._log.info("Beacon status is %s.  Discarded message intended for uplink: %r",
                           self.beacon_status, payload)
            return

        # Push the outgoing message to outgoing deque
        self._log.debug("Queueing write to serial port: %r", payload)
        self.__output_buffer.append(payload)

        # Register our interest in writing things
        loop = asyncio.get_event_loop()
        loop.add_writer(self.serial.fileno(), self.__write_serial)

    def __write_serial(self):
        r"""
        Private method for writing to the serial port when able.

        @return:
        """

        # Queue is empty -- remove the writer callback from the event loop and exit.
        if len(self.__output_buffer) == 0:
            loop = asyncio.get_event_loop()
            loop.remove_writer(self.serial.fileno())
            return

        next_message = self.__output_buffer.popleft()  # deque.append()/popleft() = FIFO
        self._log.info("SERIAL_TX: %r", next_message)
        self.serial.write(next_message.encode())

    def __read_serial(self):
        r"""
        Private method for reading from serial port when data's available.
        @return:
        """

        # Read some data
        data = self.serial.read(2048)
        self._log.info("SERIAL_RX: %r", data)

        # Handler attached?
        if self.protocol is not None:
            # And still available?
            protocol = self.protocol()
            # Then act on the new email
            # TODO:  Allow yielding to handler?  If handler is a coroutine or task?
            if protocol is not None:
                try:
                    protocol.message_received(data)
                except:
                    self._log.exception("Unable to process serial data: %r", str(data))
                    
    @asyncio.coroutine
    def __poll_beacon(self, interval=120, mailbox_check=False):
        r"""
        Private method to trigger beacon to check for MT msgs on the 
        Iridium gateway ("mailbox check") and perform all other 
        synchronous actions.

        @param interval:  Interval (seconds)
        @type interval: int

        Not intended for use by user.  Use `start_polling()`
        """

        if self.serial.closed:
            raise RuntimeError("The serial port is closed")

        # Loop to perform synchronous actions on beacon.
        while True:

            # Synchronous mailbox check.   Perform only if enabled and beacon not submerged.
            if mailbox_check and self.beacon_status in ['NORMAL','STARTUP','SURFACED'] \
               and not self.skip_sync_flag:
                # Perform empty mailbox check.  Available as of f/w 3595
                self.sendto_beacon("","","$iMsgChk\r")
                self._log.debug("Waiting %d seconds before next MT mailbox check", interval)

            # Query beacon status (startup, normal, submerged, surfaced, etc.)
            # Resending $diag 1 enables recovery from a beacon power-cycle.
            self.sendto_beacon("","",'$diag 1\r$status\r')

            # Reset skip flag.
            self.skip_sync_flag = False

            try:
                yield from asyncio.sleep(interval)
            except asyncio.CancelledError:
                break

    def start_polling(self, interval=120, mailbox_check=False):
        r"""
        Start loop to perform synchronous actions.

        To stop polling, call the `stop_polling` function.
        """
        if self.__poll_future is not None:
            raise RuntimeError("Polling already started.")

        self.__poll_future = asyncio.ensure_future(self.__poll_beacon(interval, mailbox_check))

    def stop_polling(self):
        r"""
        Cancel all pending operations.
        @return:
        """

        if self.__poll_future is not None:
            self.__poll_future.cancel()
            self.__poll_future = None




@asyncio.coroutine
def create_xeos_serial_endpoint(
        protocol_factory, port: (str), baud: (int)):
    r"""
    Creates an Serial endpoint for use with Xeos Iridium Beacons.

    @param protocol_factory:  Callable returning a protocol instance

    @param port:  Serial port connected to Iridium
    @type str: e.g. /dev/ttyS0

    @param baud:  Baud rate
    @type int: e.g. 57600

    @return: (XeosSerialTransport, protocol)
    """

    # Create the transport and protocol
    serialport = Serial(port, baud)
    transport = XeosSerialTransport(serialport)
    protocol = protocol_factory()

    # Connect the transport up to the email servers
    yield from transport.connect(protocol=protocol)

    # Attach the transport to the protocol
    protocol.connection_made(transport)

    return (transport, protocol)
