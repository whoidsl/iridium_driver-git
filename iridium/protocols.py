import asyncio
import weakref
import email
import logging

from iridium import parsing


class XeosProtocol(asyncio.BaseProtocol):
    """

    """

    def __init__(self):
        r"""

        """

        self.transport = None
        self.handler = None

        self._log = logging.getLogger(__name__)

    def attach_handler(self, handler):
        r"""

        @param handler:
        @return:
        """

        self.handler = handler

    def connection_made(self, transport):
        r"""

        @param transport:
        @return:
        """

        self.transport = weakref.ref(transport)

    def connection_lost(self, exc):
        r"""

        @param exc: Reason for losing the connection.
        @return: None or Exception

        Returns `None` if the underlying transport is closed
        normally.
        """

        # Remove the transport reference
        self.transport = None

        # Transport was closed normally
        if exc is None:
            return

        raise exc

    def message_received(self, msg):
        r"""
        Called from the attached transport for each email received.

        @param msg:
        @return:
        """

        self.handler(msg)


class CommandProtocol(asyncio.DatagramProtocol):
    r"""
    Simple UDP-based protocol for controlling a Xeos service
    """

    def __init__(self):
        self.transport = None
        self.handler = None

    def connection_made(self, transport):
        r"""
        Called when the connection is made.

        @param transport: UDP transport
        @return:
        """

        self.transport = weakref.ref(transport)

    def attach_handler(self, handler):
        r"""

        @param handler:
        @return:
        """

        self.handler = handler

    def connection_lost(self, exc):
        r"""
        Called when the underlying transport is closed or lost.
        @param exc:
        @return:
        """

        self.transport = None

    def datagram_received(self, data, addr):
        r"""

        @param data:
        @param addr:
        @return:
        """

        print("Received %s" % (repr(data)))
        self.handler(data.decode())

    def error_received(self, exc):
        r"""

        @param exc:
        @return:
        """

        print("Received error!")
        import traceback
        traceback.print_exc()
