function async_loopback_performance(topudp,subudp)
% async_loopback_b2b(topudp,subudp)
%
% topudp: pyudpterm log from topside beacon from revised test G.2 of
% test plan using internal loopbakc feature of iridium driver.
%
%
% Revision History
% 2017-04-18    mvj    Created.
% 2017-05-04    mvj    Update for new f/w with empty message check.


% Process topside log
eval(sprintf('!grep ^URX %s > /tmp/top.urx',topudp));
eval(sprintf('!grep ^UTX %s > /tmp/top.utx',topudp)); % should be empty for this test.
[top.rx.t,top.rx.type,top.rx.tt] = read_pyudp('/tmp/top.urx');
[top.tx.t,top.tx.type,top.tx.tt] = read_pyudp('/tmp/top.utx');

% Process subsea log
eval(sprintf('!grep ^URX %s > /tmp/sub.urx',subudp));
eval(sprintf('!grep ^UTX %s > /tmp/sub.utx',subudp)); % should be empty for this test.
[sub.rx.t,sub.rx.type,sub.rx.tt] = read_pyudp('/tmp/sub.urx');
[sub.tx.t,sub.tx.type,sub.tx.tt] = read_pyudp('/tmp/sub.utx');

% Add gps tx ymd.  
[~,fn] = fileparts(topudp)
[yy,mm,dd] = strread(fn,'%4d%2d%2d%*[^\n]');
ig = top.rx.type == 2;
for n=find(ig)'
  [~,~,~,hh,nn,ss] = sec_to_ymdhms(top.rx.tt(n));
  top.rx.tt(n) = ymdhms_to_sec(yy,mm,dd,hh,nn,ss);
  
  % rollover (only handles one day).
  if top.rx.tt(n) > top.rx.t(n)
    top.rx.tt(n) = top.rx.tt(n)-3600*24;
  end
end


% basic timeseries plot.
figure(1); clf reset;
ir = top.rx.type == 3; % response
ig = top.rx.type == 2; % gps 
iq = top.rx.type == 4; % query (from loopback.  UDP rx time is overwritten.)
im = top.rx.type == 1; % sync uplink
dump
plot(top.rx.t,cumsum(im), top.rx.t,cumsum(ig),top.rx.t,cumsum(iq), ...
    top.rx.t,cumsum(ir));
legend('Sync. Uplink','GPS','Query','Response')
grid on;

tticklabel('abs',600);


figure(2); clf reset;
for n=1:length(top.rx.t)
  colr =0;
  if top.rx.type(n) == 4  % query (from lb), top->sub
    line([top.rx.tt(n) top.rx.t(n)],[1 0],'color','b');  % sub->top
    line(top.rx.t(n),0,'marker','.','color','b','markersize',20);    
  elseif top.rx.type(n) == 2 % GPS, sub->top
    line([top.rx.t(n) top.rx.tt(n)],[1 0],'color','k');  % sub->top
    line(top.rx.t(n),1,'marker','.','color','k','markersize',20);    
  elseif top.rx.type(n) == 3 % response, sub->top
    colr = [0 0.5 0];
    line([top.rx.t(n) top.rx.tt(n)],[1 0],'color',colr);  % sub->top
    line(top.rx.t(n),1,'marker','.','color',colr,'markersize',20);    
  elseif top.rx.type(n) == 1 % sync uplink, sub->top
    line([top.rx.t(n) top.rx.tt(n)],[1 0],'color','r');  % sub->top
    line(top.rx.t(n),1,'marker','.','color','r','markersize',20);    
  end
end

ylim([-0.1 1.1]);
set(gca,'ytick',[0 1],'yticklabel',{'SUB','TOP'},'xgrid','on');
tticklabel('abs',600);
h(1) = line(NaN,NaN,'color','k','marker','.','markersize',20);
h(2) = line(NaN,NaN,'color','b','marker','.','markersize',20,'linestyle','-');
h(3) = line(NaN,NaN,'color',[0 0.5 0],'marker','.','markersize',20,'linestyle','-');
h(4) = line(NaN,NaN,'color','r','marker','.','markersize',20,'linestyle','-');

legend(h,'GPS','Query','Response','Sync. Uplink');

% latency histograms
figure(3); clf reset;
bins = [0:60:600];
subplot(221)
is = top.rx.type == 1;
histogram((top.rx.t(is) - top.rx.tt(is))/60,bins/60);
title('Latency, Sync. Uplink')
xlabel('minutes')
subplot(222)
ig = top.rx.type == 2;
histogram((top.rx.t(ig) - top.rx.tt(ig))/60,bins/60);
title('Latency, GPS Uplink')
xlabel('minutes')
subplot(223)
is = top.rx.type == 4;
histogram((top.rx.t(is) - top.rx.tt(is))/60,bins/60);
title('Latency, Async. Downlink')
xlabel('minutes')
subplot(224)
is = top.rx.type == 5;
histogram((top.rx.t(is) - top.rx.tt(is))/60,bins/60);
title('Latency, Loopback')
xlabel('minutes')

% could do drop stats.   Would have to look at UTX logs for this.

dump;

function [t,type,tt] = read_pyudp(f)

[yy,mm,dd,hh,nn,ss,str] = textread(f, ...
    'U%*cX %d/%d/%d %d:%d:%f %[^\n]');

t = ymdhms_to_sec(yy,mm,dd,hh,nn,ss);
tt = NaN*t;
type = NaN*t;

nnn = 0;
N = length(str);
for n=1:N
  if strfind(str{n},'ILB')  % loopback
    type(n) = 3; % uplink (response) portion.
    [yy,mm,dd,hh,nn,ss] = strread(str{n},'ITS %d-%d-%d %d:%d:%d ILB ITS %*d-%*d-%*d %*d:%*d:%*d%*[^\n]');
    tt(n) = ymdhms_to_sec(yy,mm,dd,hh,nn,ss);    
    nnn = nnn+1;
    type(N+nnn) = 4; % downlink (query) portion;
    [yy,mm,dd,hh,nn,ss] = strread(str{n},'ITS %*d-%*d-%*d %*d:%*d:%*d ILB ITS %d-%d-%d %d:%d:%d%*[^\n]');
    tt(N+nnn) = ymdhms_to_sec(yy,mm,dd,hh,nn,ss);        
    t(N+nnn) = tt(n); % rx time of query subsea.
    nnn = nnn+1;
    type(N+nnn) = 5; % total lb time.
    tt(N+nnn) = tt(N+nnn-1); % tx time of query topside.
    t(N+nnn) = t(n); % rx time of response topside.
  elseif strmatch('ITS',str{n})
    type(n) = 1;
    [yy,mm,dd,hh,nn,ss] = strread(str{n},'ITS %d-%d-%d %d:%d:%d%*[^\n]');
    tt(n) = ymdhms_to_sec(yy,mm,dd,hh,nn,ss);
  elseif strmatch('$GPGGA',str{n})
    type(n) = 2;
    [hh,nn,ss] = strread(str{n},'$GPGGA,%2d%2d%2d,%*[^\n]');
    tt(n) = ymdhms_to_sec(0,0,0,hh,nn,ss);
  else
    type(n) = 0; % unknown.
  end
end

[t,is] = sort(t);
tt = tt(is);
type = type(is);
