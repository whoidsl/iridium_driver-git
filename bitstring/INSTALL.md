Install bitstring package
=========================

iridium_driver depends on the bitstring package to decode the binary Xeos GPS format.  bitstring is not available as a .deb package for Ubuntu 14.04.  It can be installed using pip3 or using the zipped archive contained in this directory.

Local install:
--------------

Unzip the package and install it.

'unzip bitstring-3.1.5.zip'
'cd bitstring-3.1.5'
'sudo python3 setup.py install'


Installation with pip3:
-----------------------

Requires an Internet connection.

'sudo pip3 install bitstring'
