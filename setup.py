from setuptools import setup, find_packages
import sys

if sys.version_info < (3, 4):
    sys.exit('Requires Python 3.4 or newer')

setup(
    name="IridiumDriver",
    version="0.1",
    packages=find_packages('iridium'),
    #scripts=['iridium_old.py'],

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=['pyserial>=2.6', 'beautifulsoup4>=4.2', 'bitstring>=3.1'],

    # Dependencies for testing
    tests_require=['pytest>=2.5', 'pytest-asyncio'],

    # package_data={
    #     # If any package contains *.txt or *.rst files, include them:
    #     '': ['*.txt', '*.rst'],
    #     # And include any *.msg files found in the 'hello' package, too:
    #     'hello': ['*.msg'],
    # },

    entry_points={
        'console_scripts':
            'iridium_driver = iridium.daemon:main'
    },

    # metadata for upload to PyPI
    author="Theo Guerin",
    author_email="tguerin@whoi.edu",
    maintainer="Zac Berkowitz",
    maintainer_email="zberkowitz@whoi.edu",
    description="A python package for working with Xeos iridium beacons"
    # license="PSF",
    # keywords="hello world example examples",
    #url="http://example.com/HelloWorld/",   # project home page, if any

    # could also include long_description, download_url, classifiers, etc.
)
