Sentry Iridium Driver
=====================

Based on original work by Theo Guerin, tguerin@whoi.edu.

What it Does
------------

Provides a `UDP<->Iridium` link.  Iridium beacons can either be directly
attached via a serial port or remotely though the Xeos Forward email service.

Messages received on the driver's listening UDP port will be transmitted *verbatim*
to the iridium beacon.  Data received from and iridium beacon (either on the serial port
or from a parsed email) will be forwarded to the specified `remote` address.

Python Requirements
-------------------

- Python 3 >= 3.4.3 (`python3`)
- pyserial (`python3-serial`)
- BeautifulSoup >= 4.2 (`python3-bs4`)

Apt one-liner: `sudo apt-get install -y python3 python3-serial python3-bs4`

Additional requirements not available as a package for 14.04:
- bitstring

Install with pip (requires internet): sudo pip3 install bitstring

Optional Requirements for Testing:

- py.test (`python3-pytest`)

Testing
-------

To run the few tests you'll need `py.test` installed.  Then run the following:

`py.test-3 --doctest-modules iridium`

Installing
----------

To use the `iridium_driver` you must install this package. The easiest way to do this
is running:

`sudo pip3 install -e .` 

from the directory containing this `README.md`.  This will install al link to
the `iridium_driver` entry point into a location in the system's `$PATH`.

*You do NOT need to re-install after modifying the source code if you use this method*

You can remove the link at any time with `sudo pip uninstall IridiumDriver`

Usage
-----

Run `iridium_driver --help` to get a list of options that can be passed to the driver from
the command line.  Alternatively, an ini-style config file may be specified with the 
`--config` option.  See `example_config.ini` for more information.
